package isima.F2.controller;


import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import isima.f2.controllers.PoubelleController;
import isima.f2.model.Poubelle;
import isima.f2.services.ImpPoubelle;

@WebMvcTest(PoubelleController.class)
public class PoubelleControllerTest {
	
	@MockBean
	private ImpPoubelle service;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void verificationGetPoubellesMethode() throws Exception { 
		List<Poubelle> listRes = new ArrayList();
		listRes.add(new Poubelle());
		//Return null car Il n'y pas une initialisation de constructeur
		when(service.getPoubelles()).thenReturn(listRes);
		this.mockMvc.perform(get("/poubelles/")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"id\":null")));
	}
	

}
