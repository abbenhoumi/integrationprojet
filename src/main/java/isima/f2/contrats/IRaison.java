package isima.f2.contrats;

import java.util.List;
import java.util.Optional;

import isima.f2.model.Raison;

public interface IRaison {
	public Raison ajouterRaison(Raison raison);
	public void deleteRaison(Long id);
	public Raison modifierRaison(Raison raison);
	public Optional<Raison> getRaison(Long id);
	public List<Raison> getRaisons();
}
