package isima.f2.contrats;

import java.util.List;
import java.util.Optional;

import isima.f2.model.Action;

public interface IAction {
	
		public Action ajouterAction(Action action);
		public void deleteAction(Long id);
		public Action modifierAction(Action action);
		public Optional<Action> getAction(Long id);
		public List<Action> getActions();
		
}
