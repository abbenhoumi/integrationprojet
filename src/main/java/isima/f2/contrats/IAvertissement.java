package isima.f2.contrats;

import java.util.List;
import java.util.Optional;

import isima.f2.model.Avertissement;

public interface IAvertissement {
	public Avertissement ajouterAvertissement(Avertissement avertissement);
	public void deleteAvertissement(Long id);
	public Avertissement modifierAvertissement(Avertissement avertissement);
	public Optional<Avertissement> getAvertissement(Long id);
	public List<Avertissement> getAvertissements();
}
