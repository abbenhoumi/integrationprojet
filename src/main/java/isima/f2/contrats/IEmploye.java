package isima.f2.contrats;

import java.util.List;
import java.util.Optional;
import isima.f2.model.Employe;

public interface IEmploye {
	public Employe ajouterEmploye(Employe employe);
	public void deleteEmploye(Long id);
	public Employe modifierEmploye(Employe employe);
	public Optional<Employe> getEmploye(Long id);
	public List<Employe> getEmployes();
}
