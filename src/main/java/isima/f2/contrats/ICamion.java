package isima.f2.contrats;

import java.util.List;
import java.util.Optional;

import isima.f2.model.Camion;

public interface ICamion {
	public Camion ajouterCamion(Camion camion);
	public void deleteCamion(Long id);
	public Camion modifierCamion(Camion camion);
	public Optional<Camion> getCamion(Long id);
	public List<Camion> getCamions();
}
