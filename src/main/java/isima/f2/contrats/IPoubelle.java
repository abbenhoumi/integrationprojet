package isima.f2.contrats;

import java.util.List;
import java.util.Optional;

import isima.f2.model.Poubelle;

public interface IPoubelle {
	public Poubelle ajouterPoubelle(Poubelle poubelle);
	public void deletePoubelle(Long id);
	public Poubelle modifierPoubelle(Poubelle poubelle);
	public Optional<Poubelle> getPoubelle(Long id);
	public List<Poubelle> getPoubelles();
	public void viderPoubelle(long id);
	public void updateContenuPoubelle(long id, double contenu);
	public void creerPoubelle(Poubelle nouvellePoubelle);
}
