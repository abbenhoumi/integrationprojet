package isima.f2;

import isima.f2.services.ThreadSimulation;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RamassagePoubleDepotApplication {

	public static void main(String[] args) {
		SpringApplication.run(RamassagePoubleDepotApplication.class, args);
		System.out.println("=======================>Le nom du thread principal est " + Thread.currentThread().getName());
		ThreadSimulation t = new ThreadSimulation();
		t.start();
	}

}
