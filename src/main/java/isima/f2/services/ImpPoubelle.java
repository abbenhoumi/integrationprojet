package isima.f2.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import isima.f2.contrats.IPoubelle;
import isima.f2.dao.IPoubelleDAO;
import isima.f2.model.Poubelle;

@Service
public class ImpPoubelle implements IPoubelle{
	
	@Autowired
	IPoubelleDAO poubelleDAO ;

	public ImpPoubelle(){

    }

	@Override
	public Poubelle ajouterPoubelle(Poubelle poubelle) {
		return poubelleDAO.save(poubelle);
	}

	@Override
	public void deletePoubelle(Long id) {
		
	}

	@Override
	public Poubelle modifierPoubelle(Poubelle poubelle) {
		return null;
	}

	@Override
	public Optional<Poubelle> getPoubelle(Long id) {
		return poubelleDAO.findById(id);
	}

	@Override
	public List<Poubelle> getPoubelles() {
		return poubelleDAO.findAll();
	}
	
	@Override
	public void viderPoubelle(long id){
		poubelleDAO.updatePoubelleSetCapacityZero(id);
	}

    @Override
    public void updateContenuPoubelle(long id, double contenu) {
        poubelleDAO.updatePoubelleSetCapacity(id,contenu);
    }

	@Override
	public void creerPoubelle(Poubelle nouvellePoubelle) {
		poubelleDAO.save(nouvellePoubelle);
	}
}
