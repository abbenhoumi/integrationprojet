package isima.f2.services;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import isima.f2.contrats.IAvertissement;
import isima.f2.dao.IAvertissementDAO;
import isima.f2.model.Avertissement;

public class ImpAvertissement implements IAvertissement {
	
	@Autowired
	private IAvertissementDAO avertissementDAO ; 

	@Override
	public Avertissement ajouterAvertissement(Avertissement avertissement) {
		return avertissementDAO.save(avertissement);
	}

	@Override
	public void deleteAvertissement(Long id) {
		avertissementDAO.deleteById(id);		
	}

	@Override
	public Avertissement modifierAvertissement(Avertissement avertissement) {
		deleteAvertissement(avertissement.getId());
		return ajouterAvertissement(avertissement);
	}

	@Override
	public Optional<Avertissement> getAvertissement(Long id) {
		return avertissementDAO.findById(id);
	}

	@Override
	public List<Avertissement> getAvertissements() {
		return avertissementDAO.findAll();
	}

}
