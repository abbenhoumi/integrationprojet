package isima.f2.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import isima.f2.dao.IActionDAO;
import isima.f2.contrats.IAction;
import isima.f2.model.Action;

@Service
public class ImpAction implements IAction  {
	
	@Autowired
	private IActionDAO actionDao ;

	@Override
	public Action ajouterAction(Action action) {
		return actionDao.save(action);
	}

	@Override
	public void deleteAction(Long id) {
		 actionDao.deleteById(id);
	}

	@Override
	public Action modifierAction(Action action) {
		deleteAction(action.getId());
		return ajouterAction(action);
	}

	@Override
	public Optional<Action> getAction(Long id) {
		return actionDao.findById(id);
	}

	@Override
	public List<Action> getActions() {
		return actionDao.findAll();
	} 
	
 
}
