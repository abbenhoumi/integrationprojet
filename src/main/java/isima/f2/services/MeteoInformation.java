package isima.f2.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;


/** Cette classe ets pour récuperer les informations de la méteo
 * @author Abdelkhalek BENHOUMINE
 *
 */
public class MeteoInformation {
	
	
	private MeteoInformation() {
		//Rien juste pour assurer la qualite de code
	}
	private static final String URL = "https://samples.openweathermap.org/data/2.5/weather?zip=63000,fr&appid=b6907d289e10d714a6e88b30761fae22";

	
	 private static String readAll(Reader rd) throws IOException {
		    StringBuilder sb = new StringBuilder();
		    int cp;
		    while ((cp = rd.read()) != -1) {
		      sb.append((char) cp);
		    }
		    return sb.toString();
		  }
	 
	 public static JSONObject readJsonFromUrl(String url) throws IOException{
		    InputStream is = new URL(url).openStream();
		    try(  BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));) {
		      String jsonText = readAll(rd);
		      return new JSONObject(jsonText);
		    } finally {
		      is.close();
		    }
		  }
	 public static String getInformation() throws IOException {
		    JSONObject json = readJsonFromUrl(URL);
		    JSONArray weather = (JSONArray)json.get("weather");
		    Iterator<Object> iterator = weather.iterator();
            while (iterator.hasNext()) {
            	JSONObject weatherJson = (JSONObject)iterator.next();
        	    return weatherJson.get("main").toString() ; 
            	
            }
            return null ; 
		  }
		
}
