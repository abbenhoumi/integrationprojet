package isima.f2.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import isima.f2.contrats.IEmploye;
import isima.f2.dao.IEmployeDAO;
import isima.f2.model.Employe;

@Service
public class ImpEmploye implements IEmploye {

	@Autowired
	private IEmployeDAO employeService; 
	
	@Override
	public Employe ajouterEmploye(Employe employe) {
		return employeService.save(employe);
	}

	@Override
	public void deleteEmploye(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Employe modifierEmploye(Employe employe) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Employe> getEmploye(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employe> getEmployes() {
		// TODO Auto-generated method stub
		return null;
	}

}
