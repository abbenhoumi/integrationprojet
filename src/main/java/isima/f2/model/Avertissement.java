package isima.f2.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Avertissement implements Serializable {

		private static final long serialVersionUID = 1L;
		
		@Id
		@GeneratedValue
		private Long id ;

		public Avertissement() {
			super();
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		} 
		
		
}
