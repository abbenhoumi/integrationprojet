package isima.f2.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Raison {
	@Id
	@GeneratedValue
	private Long id ; 
	private String raisontext ;
	
	public Raison() {
		super();
	}
	public Raison(Long id, String raisontext) {
		super();
		this.id = id;
		this.raisontext = raisontext;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRaisonText() {
		return raisontext;
	}
	public void setRaisonText(String raison) {
		this.raisontext = raison;
	}
	@Override
	public String toString() {
		return "Raison [id=" + id + ", raison=" + raisontext + "]";
	} 
	
	
	
	
	
}
