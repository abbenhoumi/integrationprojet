package isima.f2.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class CompteConnexionEmploye implements Serializable{
	
		private static final long serialVersionUID = 1L;
		
		@Id
		@GeneratedValue(strategy= GenerationType.AUTO)
		private Long id ; 
		private String codeConnexion ; 
		private String password ; 
		
		@OneToOne
		private Employe employe ;
		
		
		public CompteConnexionEmploye(Long id, String codeConnexion, String password, Employe employe) {
			super();
			this.id = id;
			this.codeConnexion = codeConnexion;
			this.password = password;
			this.employe = employe;
		}

		public CompteConnexionEmploye() {
			super();
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCodeConnexion() {
			return codeConnexion;
		}

		public void setCodeConnexion(String codeConnexion) {
			this.codeConnexion = codeConnexion;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public Employe getEmploye() {
			return employe;
		}

		public void setEmploye(Employe employe) {
			this.employe = employe;
		} 
		
		
}
