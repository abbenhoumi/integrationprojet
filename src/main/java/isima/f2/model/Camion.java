package isima.f2.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/***
 * 
 * @author abdelkhalek benhoumine
 * Entity de la classe Camion
 * Contient les information d'un camion genre : id , matricule , marque ...
 *
 */
@Entity
public class Camion implements Serializable{


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	private String matricule ; 
	private String marque ; 
	private Long capaciteMaximal ;
	
	
	public Camion(Long id, String matricule, String marque, Long capaciteMaximal) {
		super();
		this.id = id;
		this.matricule = matricule;
		this.marque = marque;
		this.capaciteMaximal = capaciteMaximal;
	}
	public Camion() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	public Long getCapaciteMaximal() {
		return capaciteMaximal;
	}
	public void setCapaciteMaximal(Long capaciteMaximal) {
		this.capaciteMaximal = capaciteMaximal;
	}
	@Override
	public String toString() {
		return "Camion [id=" + id + ", matricule=" + matricule + ", marque=" + marque + ", capaciteMaximal="
				+ capaciteMaximal + "]";
	} 
	
	
}
