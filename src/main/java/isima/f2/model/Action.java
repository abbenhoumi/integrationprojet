package isima.f2.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Action implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;

	@Enumerated(EnumType.STRING)
	private TypeAction typeAction;

	public Action() {
		super();
	}

	public Action(Long id, TypeAction typeAction) {
		super();
		this.id = id;
		this.typeAction = typeAction;
	}

	public TypeAction getTypeAction() {
		return typeAction;
	}

	public void setTypeAction(TypeAction typeAction) {
		this.typeAction = typeAction;
	}

	public Long getId() {
		return id;
	}

}
