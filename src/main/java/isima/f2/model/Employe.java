package isima.f2.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Employe implements Serializable{

		private static final long serialVersionUID = 1L;
		@Id
		@GeneratedValue
		private Long id ; 
		private String nom ; 
		private String prenom  ; 
		private Date premierJour  ; 
		private String poste ;
		
		//Chaque employe peut avoir des avertissement
		@OneToMany
		private List<Avertissement> avertissements; 
		
		
		public Employe() {
			super();
		}

		public Employe(Long id, String nom, String prenom, Date premierJour, String poste) {
			super();
			this.id = id;
			this.nom = nom;
			this.prenom = prenom;
			this.premierJour = premierJour;
			this.poste = poste;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		public Date getPremierJour() {
			return premierJour;
		}
		public void setPremierJour(Date premierJour) {
			this.premierJour = premierJour;
		}
		public String getPoste() {
			return poste;
		}
		public void setPoste(String poste) {
			this.poste = poste;
		} 
		
		@Override
		public String toString() {
			return "Employe [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", premierJour=" + premierJour
					+ ", poste=" + poste + "]";
		}
		
}
