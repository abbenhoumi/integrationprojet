package isima.f2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import isima.f2.model.Action;

public interface IActionDAO extends JpaRepository<Action, Long>{

}
