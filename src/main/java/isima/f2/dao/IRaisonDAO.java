package isima.f2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import isima.f2.model.Raison;

public interface IRaisonDAO extends JpaRepository<Raison, Long>{
}
