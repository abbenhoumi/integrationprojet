package isima.f2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import isima.f2.model.CompteConnexionEmploye;

public interface ICompteConnexion extends JpaRepository<CompteConnexionEmploye, Long>{
}
