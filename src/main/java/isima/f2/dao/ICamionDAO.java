package isima.f2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import isima.f2.model.Camion;

public interface ICamionDAO extends JpaRepository<Camion, Long> {

}
