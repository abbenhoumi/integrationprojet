package isima.f2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import isima.f2.model.Employe;

public interface IEmployeDAO extends JpaRepository<Employe, Long> {

}
