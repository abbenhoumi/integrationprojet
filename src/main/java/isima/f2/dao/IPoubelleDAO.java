package isima.f2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import isima.f2.model.Poubelle;

public interface IPoubelleDAO extends JpaRepository<Poubelle, Long> {
	
    @Modifying
    @Transactional
    @Query("UPDATE Poubelle p SET p.contenu = 0 WHERE p.id = :id")
    int updatePoubelleSetCapacityZero(@Param("id") long id);


    @Modifying
    @Transactional
    @Query("UPDATE Poubelle p SET p.contenu = :contenu WHERE p.id = :id")
    int updatePoubelleSetCapacity(@Param("id") long id, @Param("contenu") double contenu);

}
