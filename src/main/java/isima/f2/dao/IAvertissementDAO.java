package isima.f2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import isima.f2.model.Avertissement;

public interface IAvertissementDAO extends JpaRepository<Avertissement, Long>{

}
