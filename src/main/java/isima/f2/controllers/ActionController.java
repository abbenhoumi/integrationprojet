package isima.f2.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import isima.f2.model.Action;
import isima.f2.services.ImpAction;

@RestController
@RequestMapping("actions")
public class ActionController {

	@Autowired
	private ImpAction actions;

	@GetMapping("/")
	public List<Action> getActions() {
		return actions.getActions();
	}

}
