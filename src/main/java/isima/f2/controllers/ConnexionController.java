package isima.f2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import isima.f2.dao.ICompteConnexion;
import isima.f2.dao.IEmployeDAO;
import isima.f2.model.Employe;

@RestController
@RequestMapping("connexion")
public class ConnexionController {

	@Autowired
	private ICompteConnexion connexion ; 
	@Autowired
	private IEmployeDAO empploye ; 
	
	@GetMapping("/")
    public ResponseEntity customHandleNotFound(Exception ex, WebRequest request) {
        return new ResponseEntity<>("errors", HttpStatus.NOT_FOUND);

    }
	
	@GetMapping("/login")
	@ResponseBody
	public Employe login(@RequestParam String id) {
	    if(empploye.findById(Long.parseLong(id)).isPresent()) {
	    	return empploye.findById(Long.parseLong(id)).get();
	    }else{
	    	return null ;
	    }
	}
}
