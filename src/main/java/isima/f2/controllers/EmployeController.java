package isima.f2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import isima.f2.model.Employe;
import isima.f2.services.ImpEmploye;

@RestController
@RequestMapping("employes")
public class EmployeController {
	
	@Autowired
	private ImpEmploye employeService;

	@PostMapping(value="/saveemploye", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Employe savePoubelle(@RequestBody Employe employe) {
		return employeService.ajouterEmploye(employe);
	}
}
