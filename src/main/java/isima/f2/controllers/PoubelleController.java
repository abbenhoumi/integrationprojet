package isima.f2.controllers;

import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import isima.f2.model.Poubelle;
import isima.f2.services.ImpPoubelle;

@RestController
@RequestMapping("poubelles")
@CrossOrigin(origins = "http://localhost:4200")
public class PoubelleController {

	@Autowired
	private ImpPoubelle poubelles;

	@GetMapping(value="/", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Poubelle> getPoubelles() {
		return poubelles.getPoubelles();
	}

	@PostMapping(value="/savepoubelle", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Poubelle savePoubelle(@RequestBody Poubelle poubelle) {
		return poubelles.ajouterPoubelle(poubelle);
	}
	
	@PostMapping ("/vider")
	@ResponseBody
	public String viderPoubelle(@RequestParam Map <String, String> allParams) {
		long id =  Long.parseLong(allParams.get("id"));
		poubelles.viderPoubelle(id);
	    return "La poubelle a été vidée avec succes";
	}
	
	@PostMapping ("/testpost")
	@ResponseBody
	public String testpost(@RequestParam Map <String, String> allParams) {
	    return "Les paramètres sont"+ allParams.entrySet ();
	}

	@PostMapping ("/nouvelle")
	@ResponseBody
	public String creerPoubelle(@RequestParam Map <String, String> allParams) {
		String poubelleJSON =  allParams.get("poubelle");
		poubelleJSON = poubelleJSON.replace("Poubelle","");
		System.out.println("Poubellllllllllllleeeeeeeee ******** : " + poubelleJSON);
		Gson gson = new Gson();
		Poubelle nouvellePoubelle = gson.fromJson(poubelleJSON, Poubelle.class);
		//update poubelle id ===> last id + 1
		nouvellePoubelle.setId((long)(poubelles.getPoubelles().size()+1));
		//nouvellePoubelle.setId(6L);
		System.out.println("**************** " + nouvellePoubelle.toString() + "****************");
		poubelles.creerPoubelle(nouvellePoubelle);
		return "La nouvelle poubelle a été créée avec succes";
	}
}
